# Custom files for DooM Emacs

These files are modified to taste.
Read first before deploying!

# Deploying
DooM Emacs requires 3 steps to fully work

First, install the dependencies

~~~shell
./arch_bootstrap.sh
~~~

Then, install the npm dependencies

~~~shell
./npm_bootstrap
~~~

Now install DooM Emacs the official way.
And once that is installed, deploy the config files.

~~~shell
./deploy.sh
~~~

Files will be sent to their respective destinations as specified 
by the Arch Wiki.

If you need them elsewhere, modify deploy.sh to your needs.

# When updating
When updating your configuration, only deploy.sh is required to run,
arch\_bootsrap.sh and npm\_bootstrap.sh are only needed before the first
run ever in your system.

