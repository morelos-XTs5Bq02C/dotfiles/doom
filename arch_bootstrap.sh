#!/bin/sh

# Needed by lsp
pacman -Sy npm

# Needed by org
pacman -Sy graphviz

# Needed by python
pacman -Sy python-isort python-pipenv python-pytest

# Needed by sh
pacman -Sy shellcheck

# Needed by web
pacman -Sy tidy stylelint

