;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

(defgroup c13l nil
  "Customizations related to me >.<"
  :group 'convenience
  :version "29.1")

(setq color-scarlet "#ff2400") ;; Shade of red
(setq color-malachite "#0bda51") ;; Shade of green
(setq color-safety-orange "#ff7900") ;; Shade of orange
(setq color-greenish-yellow "#eeea62") ;; Shade of yellow
;; Colors for when downloading files (org)
(setq color-savoy-blue "#4b61d1") ;; Shade of blue
(setq color-eminence "#6c3082") ;; Shade of purple
(setq color-mellow-yellow "#f8de7e") ;; Shade of yellow

(defface c13l-priority-base
  '((t :family "Phosphor"))
  "The base face for Priorities"
  :version "29.1"
  :group 'c13l)


(defface c13l-todo-base
  '((t :background "white"
       :foreground "black"
       :weight medium
       :box (:line-width (1 . 1) :color "black")))

  "The base face for custom TODO keywords"
  :version "29.1"
  :group 'c13l)


(defface c13l-todo-ignore
  '((t :inherit (c13l-todo-base)
     :background "white smoke"))

  "The face for the IGNORE keyword"
  :version "29.1"
  :group 'c13l)


(defface c13l-todo-todo
  `((t :inherit (c13l-todo-base)
     :background ,color-scarlet))

  "The face for the TODO keyword"
  :version "29.1"
  :group 'c13l)


(defface c13l-todo-onit
  `((t :inherit (c13l-todo-base)
     :background ,color-safety-orange))

  "The face for the ONIT keyword (ongoing state)"
  :version "29.1"
  :group 'c13l)


(defface c13l-todo-wait
  `((t :inherit (c13l-todo-base)
     :background ,color-greenish-yellow))

  "The face for the queue (QUEUED, PENDING) state"
  :version "29.1"
  :group 'c13l)


(defface c13l-todo-done
  `((t :inherit (c13l-todo-base)
     :background ,color-malachite))

  "The face for the DONE keyword"
  :version "29.1"
  :group 'c13l)


  ;; '(c13l-todo-onit :weight medium :box (:line-width (1 . 1) :color "orange" :style pressed-button) :foreground "orange" :background "#343434" :inherit (org-todo))

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!
(setq doom-font (font-spec :family "Source Code Pro" :size 17 :weight 'medium))

(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))


;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-rouge)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; (load! "axy.el" "~/Git/dotfiles/emacs")
(load! "yuck-mode.el" "~/.config/doom")

;; Disable quit prompt
(setq confirm-kill-emacs nil)

;; Use ibuffer by default (requires ibuffer enabled in init.el)
(defalias 'list-buffers 'ibuffer)

;; Start ibuffer on Emacs state, as starting it with Evil Normal will interfere with iBuffer chords
;; To return to Evil Normal just press C-z
(add-to-list 'evil-emacs-state-modes 'ibuffer-mode)
;; (setq evil-emacs-state-modes (append '(ibuffer-mode) evil-emacs-state-modes))

(after! doom-modeline
  (setq doom-modeline-buffer-file-name-style 'relative-to-project))


(after! electric
  (setq
   electric-pair-delete-adjacent-pairs nil
   electric-quote-mode t))


(after! org-superstar
  (setq
   org-superstar-leading-bullet " "
   org-superstar-headline-bullets-list '("◇" "◈" "◆" "○" "◎" "◉" "⯌" "⯍" "●")))


(after! org
  (setq
   org-odd-levels-only t
   org-level-color-stars-only t
   org-allow-promoting-top-level-subtree t
   org-closed-keep-when-no-todo t
   org-hide-emphasis-markers t
   org-log-into-drawer t
   org-log-done 'time
   org-startup-folded t

   org-blank-before-new-entry '((heading . t) (plain-list-item . t))

   ;; org-todo-keywords '((sequence "TODO(s)" "|" "DONE(D)")
   ;;                     (sequence "IGNORE(i)" "QUEUED(q)" "PENDING(p)" "ON-IT(o)" "|")
   ;;                     (sequence "TRANSLATING(t)" "REVIEWING(r)" "|" "TRANSLATED(T)" "REVIEWED(R)")
   ;;                     (sequence "DOWNLOADING(d) | DOWNLOADED(D)")
   ;;                     (sequence "PURCHASE(p) SHIP(s) | SHIPPED(S) PURCHASED(P) DELIVERED(e)")
   ;;                     )

   org-todo-keyword-faces '(
                            ("IGNORE"      . c13l-todo-ignore)
                            ("TODO"        . c13l-todo-todo)
                            ("CANCEL"      . c13l-todo-todo)
                            ("DELETE"      . c13l-todo-todo)
                            ("PENDING"     . c13l-todo-wait)
                            ("QUEUED"      . c13l-todo-wait)
                            ("WAIT"        . c13l-todo-wait)
                            ("ONGOING"     . c13l-todo-onit)
                            ("ONIT"        . c13l-todo-onit)
                            ("TRANSLATING" . c13l-todo-onit)
                            ("REVIEWING"   . c13l-todo-onit)
                            ("DONE"        . c13l-todo-done)
                            ("TRANSLATED"  . c13l-todo-done)
                            ("REVIEWED"    . c13l-todo-done))))

(after! org-fancy-priorities
  (setq org-fancy-priorities-list '((?A . "")
                                    (?B . "")
                                    (?C . "")
                                    (?D . "")
                                    (?1 . "")
                                    (?2 . "")
                                    (?3 . "")
                                    (?4 . ""))))

  ;; (setq org-priority-faces `((?A :foreground ,color-scarlet)
  ;;                            (?B :foreground ,color-safety-orage)
  ;;                            (?C :foreground gray)
  ;;                            (?D :foreground gray)
  ;;                            (?1 :foreground ,color-scarlet)
  ;;                            (?2 :foreground ,color-safety-orange)
  ;;                            (?3 :foreground gray)
  ;;                            (?4 :foreground gray))))
