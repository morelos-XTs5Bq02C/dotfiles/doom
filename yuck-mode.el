;;; yuck-mode.el --- Basic syntax highlighting for Elkowars Wacky Widgets -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024
;;
;; Author:  C13L99
;; Maintainer:  C13L99
;; Created: August 07, 2024
;; Modified: August 07, 2024
;; Version: 0.0.1
;; Keywords: abbrev bib comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://gitlab.com
;; Package-Requires: ((emacs "29.4"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defvar yuck-keywords nil "yuck keywords")
(setq yuck-keywords '("defwidget"
                      "defwindow"
                      "defpoll"
                      "defvar"
                      "deflisten"))

(defvar yuck-constants nil "yuck constants")
(setq yuck-constants '("true" "false"))

;; This doesn't work, you gotta love how these people reinvent every wheel they find
;; (defvar yuck-syntax-table nil "Syntax value for Yuck")
;; (setq yuck-syntax-table
;;       (let ( (synTable (make-syntax-table)))
;;         ;; Set modify each char class
;;         (modify-syntax-entry ?: "w" synTable)
;;         synTable))

;; (set-syntax-table yuck-syntax-table)


(defvar yuck-fontlock nil "list for font lock defaults")
(setq yuck-fontlock
      (let (xkeywords-regex xconstants-regex xcomments-regex)
        ;; generate regexes
        (setq xkeywords-regex (regexp-opt yuck-keywords 'words))
        (setq xconstants-regex (regexp-opt yuck-constants 'words))
        ;; Comments
        (setq xcomments-regex ";.*$")
        ;; Builtins
        ;; The original regex was '\<:[^\r\n\t ]\+'
        ;; But emacs doesn't provide these classes ootb
        ;; So we need to rewrite it
        ;; Verbatim, on VIM regex, this means:
        ;; Match from the beggining of a word, starting with ':'
        ;; to any character that is not a space, carriage return,
        ;; end-of-line or tab
        ;; TODO: Lines like:this give a match on :this
        ;; Unfortunately, regex refuses to accept \< as what follows
        ;; is : which is not considered word-contituent, though!
        (setq xbuiltins-regex ":[^[:space:][:cntrl:]]+")
        ;; Using

        ;; note - order matters, because once colored, that part won't change. In general, put longer words first
        (list (cons xkeywords-regex 'font-lock-keyword-face)
              (cons xconstants-regex 'font-lock-constant-face)
              (cons xcomments-regex 'font-lock-comment-face)
              (cons xbuiltins-regex 'font-lock-builtin-face)
              )
        )
      )

;;; autoload
(define-derived-mode yuck-mode lisp-mode "yuck mode"
  "Major mode for editing Elkowars Wacky Widgets"

  (setq font-lock-defaults '((yuck-fontlock))))

(add-to-list 'auto-mode-alist '("\\.yuck\\'" . yuck-mode))

(provide 'yuck-mode)
;;; yuck-mode.el ends here
